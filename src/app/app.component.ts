import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {Geolocation} from "@ionic-native/geolocation";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {IabServiceProvider} from "../providers/iab-service/iab-service";
import {InAppBrowserObject} from "@ionic-native/in-app-browser";
import { AlertController } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // rootPage: any = HomePage;
  private browser: InAppBrowserObject;
  public url = 'https://yourdoc.lk/';	//'https://qa.beetlesoft.lk:8115';  // https://stage.yourdoc.lk/
  // pages: Array<{title: string, component: any}>;

  constructor(
    private platform: Platform, private androidPermissions: AndroidPermissions, private geolocation: Geolocation,
    statusBar: StatusBar, private locationAccuracy: LocationAccuracy, private iabService: IabServiceProvider,
    splashScreen: SplashScreen, public loading: LoadingController, public alertCtrl: AlertController
  ) {
    /*this.pages = [
      { title: 'Events', component: 'EventsPage' }
    ];*/

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      this.initApp().then(()=>{
        splashScreen.hide();
      });
    });

    /*let loader = this.loading.create({
       content: 'Please Wait...',
    });
    loader.present();*/
  }

  initApp(){
    let promise = new Promise((resolve, reject) => {
     this.checkGPSPermission();
      //this.openWeb('https://m.test.com');
      /*this.browser = this.iabService.iab.create('https://stage.yourdoc.lk/', '_blank', this.iabService.iabSettings);
      this.browser.on("loadstop").subscribe((res) => {
        resolve();
      }, (err) => {
        console.log("InAppBrowser Loadstop Event Error: " + err);
      });*/
    });
    return promise;
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
   // this.nav.setRoot(page.component);
  }

  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) { //console.log('If having permission show Turn On GPS dialogue');
          this.askToTurnOnGPS();
        } else { //console.log('If not having permission ask for permission')
          this.requestGPSPermission();
        }
      },
      err => {
        this.openWeb(this.url);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) { //console.log('can Request')
      } else { //console.log('Show GPS Permission Request dialogue')
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => { //console.log('call method to turn on GPS')
              this.askToTurnOnGPS();
            },
            error => { //console.log('Show alert if user click on No Thanks')
              this.openWeb(this.url);
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => { //console.log('When GPS Turned ON call method to get Accurate location coordinates')
        this.getLocationCoordinates()
      },
      error => this.openWeb(this.url)
    );
  }

  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      var urlLoc = this.url + '/?longitude=' + resp.coords.longitude + '&latitude=' + resp.coords.latitude;
      this.openWeb(urlLoc);
    }).catch((error) => {
      this.openWeb(this.url);
    });
  }

  openWeb(url) {
    this.browser = this.iabService.iab.create(url, '_blank', this.iabService.iabSettings);
    this.browser.on('exit').subscribe((e) => {
      //console.log(e.url);
      this.platform.exitApp();
    });
  }

  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}

